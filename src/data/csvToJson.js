const csv = require('csvtojson');
const fs = require('fs');

const path = require('path');

async function csvToJson(csvFilePath) {
    try {
        const jsonArray = await csv().fromFile(csvFilePath);
        const jsonResult = JSON.stringify(jsonArray, null, 4);
        return jsonResult;
        // console.log(jsonResult);
    } catch (error) {
        console.error('Error converting CSV to JSON:', error);
    }
}



module.exports = csvToJson