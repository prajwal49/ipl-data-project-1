const csvToJson = require("../data/csvToJson.js");
const fs = require("fs");
const path = require("path");


const csvFilePath = path.join(__dirname, "../data/matches.csv");

async function matchesWonPerTeamPerYear(){
   try {
    const jsonArray = await csvToJson(csvFilePath);
    const objArray = JSON.parse(jsonArray);

    let result = {}

    for(let obj of objArray){
        if(!result[obj.season]){
            result[obj.season] = {}
        }
        if(!result[obj.season][obj.winner]){
            result[obj.season][obj.winner] = 1;
        }
        else result[obj.season][obj.winner]++;
        
    }

    //converting javascript object to json file
    const jsonString = JSON.stringify(result);
    const outputFilePath = path.join(
      __dirname,
      "../public/output/matchesWonPerTeamPerYear.json"
    );
    fs.writeFileSync(outputFilePath, jsonString);
   } catch (error) {
    console.error("Error in matchesPerYear:", error);
   }
}



matchesWonPerTeamPerYear();