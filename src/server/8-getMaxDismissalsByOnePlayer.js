const csvToJson = require("../data/csvToJson.js");
const fs = require("fs");
const path = require("path");

const deliveryPath = path.join(__dirname, "../data/deliveries.csv");

const matchPath = path.join(__dirname, "../data/matches.csv");

async function getMaxDismissalsByOnePlayer(){
    const dilivery = await csvToJson(deliveryPath);
    const match = await csvToJson(matchPath);

    const deliveries = JSON.parse(dilivery);
    const matches = JSON.parse(match);

    let result = {}

    for(let i = 0;i<deliveries.length;i++){
        let batsman = deliveries[i].batsman;
        let bowler = deliveries[i].bowler;
        let playerDismissed = deliveries[i].player_dismissed;

        if(playerDismissed){
            if(!result[bowler]){
                result[bowler]= {}
            }
            if(!result[bowler][playerDismissed]){
                result[bowler][playerDismissed] = 0;
            }
            result[bowler][playerDismissed] += 1;
        }
    }
    let maxDismissals = {};
    let maximumDismiss = 0;
    let maxDismisBowler = '';
    let maxDismisBatsman = '';

    for (let bowler in result) {
        for (let batsman in result[bowler]) {
            if (result[bowler][batsman] > maximumDismiss) {
                maximumDismiss = result[bowler][batsman];
                maxDismisBatsman = batsman;
                maxDismisBowler = bowler;
            }
        }
    }

    if (!maxDismissals[maxDismisBowler]) {
        maxDismissals[maxDismisBowler] = {};
    }

    maxDismissals[maxDismisBowler][maxDismisBatsman] = maximumDismiss;
    
     //converting javascript object to json file
  const jsonString = JSON.stringify(maxDismissals);
  const outputFilePath = path.join(
    __dirname,
    "../public/output/getMaxDismissalsByOnePlayer.json"
  );
  fs.writeFileSync(outputFilePath, jsonString);
}

getMaxDismissalsByOnePlayer();

