const csvToJson = require("../data/csvToJson.js");
const fs = require("fs");
const { get } = require("https");
const path = require("path");

const deliveryPath = path.join(__dirname, "../data/deliveries.csv");

const matchPath = path.join(__dirname, "../data/matches.csv");

async function getTop10EconomicalBowlers2015() {
  const dilivery = await csvToJson(deliveryPath);
  const match = await csvToJson(matchPath);

  const deliveries = JSON.parse(dilivery);
  const matches = JSON.parse(match);
  // Filter matches for the year 2015

  let matchIds2015 = new Set();
  for (let i = 0; i < matches.length; i++) {
    if (matches[i].season === "2015") {
      matchIds2015.add(matches[i].id);
    }
  }

  let bowlerStats = {};

  // Loop through deliveries and accumulate stats for bowlers
  for (let j = 0; j < deliveries.length; j++) {
    if (matchIds2015.has(deliveries[j].match_id)) {
      let bowler = deliveries[j].bowler;
      let totalRuns = parseInt(deliveries[j].total_runs);
      let batsmanRuns = parseInt(deliveries[j].batsman_runs);
      let extraRuns = parseInt(deliveries[j].extra_runs);
      let wideRuns = parseInt(deliveries[j].wide_runs);
      let noBallRuns = parseInt(deliveries[j].noball_runs);

      if (!bowlerStats[bowler]) {
        bowlerStats[bowler] = {
          runsConceded: 0,
          ballsBowled: 0,
        };
      }

      bowlerStats[bowler].runsConceded += batsmanRuns;
      if (wideRuns === 0 && noBallRuns === 0) {
        bowlerStats[bowler].ballsBowled += 1;
      }
    }
  }

  let economy = [];
  for (let bowler in bowlerStats) {
    let runConceded = bowlerStats[bowler].runsConceded;
    let oversBowled = bowlerStats[bowler].ballsBowled / 6;
    let economyRate = runConceded / oversBowled;

    economy.push({ bowler: bowler, economyRate: economyRate });
  }
//   console.log(economy);
  let top10Bowlers = [];
  for (let bowler in economy) {
    top10Bowlers.push([bowler, economy[bowler]]);
    top10Bowlers.sort((a, b) => a[1].economyRate - b[1].economyRate);
    if (top10Bowlers.length > 10) {
      top10Bowlers.pop();
    }
  }
  let obj = Object.entries(top10Bowlers)

  //converting javascript object to json file
  const jsonString = JSON.stringify(Object.fromEntries(obj));
  const outputFilePath = path.join(
    __dirname,
    "../public/output/getTop10EconomicalBowlers2015.json"
  );
  fs.writeFileSync(outputFilePath, jsonString);
}

getTop10EconomicalBowlers2015();

// totalrun/runconceded
