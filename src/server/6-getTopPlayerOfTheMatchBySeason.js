const csvToJson = require("../data/csvToJson.js");
const fs = require("fs");
const path = require("path");

const deliveryPath = path.join(__dirname, "../data/deliveries.csv");

const matchPath = path.join(__dirname, "../data/matches.csv");

async function getTopPlayerOfTheMatchBySeason(){
    const match = await csvToJson(matchPath);
    const matches = JSON.parse(match);
    let result = {};
    for(let i = 0;i<matches.length;i++){
        let obj = matches[i]['player_of_match'];
        let season = matches[i]['season'];
        
        if(!result[season]){
            result[season] = {}
        }
        if(!result[season][obj]){
            result[season][obj] = 1;
        }
        else result[season][obj] += 1;
        
    }
    let topPlayerOfSeason = {}
    for(let obj in result){
        let players = Object.entries(result[obj]);
        players.sort((a,b) => b[1]-a[1])
        topPlayerOfSeason[obj] = players[0][0];
    }

      // converting javascript object to json file
      const jsonString = JSON.stringify(topPlayerOfSeason);
      const outputFilePath = path.join(
        __dirname,
        "../public/output/getTopPlayerOfTheMatchBySeason.json"
      );
      fs.writeFileSync(outputFilePath, jsonString);

}

getTopPlayerOfTheMatchBySeason();