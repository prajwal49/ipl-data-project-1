const csvToJson = require("../data/csvToJson.js");
const fs = require("fs");
const path = require("path");

const deliveryPath = path.join(__dirname, "../data/deliveries.csv");

const matchPath = path.join(__dirname, "../data/matches.csv");

async function calculateExtraRunsConceded2016() {
  try {
    const dilivery = await csvToJson(deliveryPath);
    const match = await csvToJson(matchPath);

    const deliveries = JSON.parse(dilivery);
    const matches = JSON.parse(match);
    // Filter matches for the year 2016

    const matchIds2016 = new Set();
    for (let i = 0; i < matches.length; i++) {
      if (matches[i].season == parseInt("2016")) {
        matchIds2016.add(parseInt(matches[i].id));
      }
    }

    const result = {};
    for (let i = 0; i < deliveries.length; i++) {
      let delivery = deliveries[i];
      if (delivery) {
        if (matchIds2016.has(parseInt(delivery.match_id))) {
          let bowlerTeam = delivery["bowling_team"];
          let extraRuns = parseInt(delivery["extra_runs"]);
          // console.log(bowlerTeam , extraRuns)
          if (!result[bowlerTeam]) {
            result[bowlerTeam] = 0;
          }

          result[bowlerTeam] += parseInt(extraRuns);
        }
      }
    }

    // converting javascript object to json file
    const jsonString = JSON.stringify(result);
    const outputFilePath = path.join(
      __dirname,
      "../public/output/extraRunsConceded2016.json"
    );
    fs.writeFileSync(outputFilePath, jsonString);
  } catch (error) {
    console.error("Error in matchesPerYear:", error);
  }
}

calculateExtraRunsConceded2016();
