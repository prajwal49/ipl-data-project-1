const csvToJson = require("../data/csvToJson.js");
const fs = require("fs");
const path = require("path");


const csvFilePath = path.join(__dirname, "../data/matches.csv");

async function matchesPerYear() {
  try {
    const jsonArray = await csvToJson(csvFilePath);
    // console.log(jsonArray)
    const objArray = JSON.parse(jsonArray);
    let result = {};
    for (let obj of objArray) {
      if (!result[obj.season]) {
        result[obj.season] = 1;
      } else result[obj.season]++;
    }

    //converting javascript object to json file
    const jsonString = JSON.stringify(result);
    const outputFilePath = path.join(
      __dirname,
      "../public/output/matchesPerYear.json"
    );
    fs.writeFileSync(outputFilePath, jsonString);
  } catch (error) {
    console.error("Error in matchesPerYear:", error);
  }
}

matchesPerYear();
