const csvToJson = require("../data/csvToJson.js");
const fs = require("fs");
const path = require("path");

const deliveryPath = path.join(__dirname, "../data/deliveries.csv");

const matchPath = path.join(__dirname, "../data/matches.csv");

async function calculateStrikeRateBySeason() {
  const dilivery = await csvToJson(deliveryPath);
  const match = await csvToJson(matchPath);

  const deliveries = JSON.parse(dilivery);
  const matches = JSON.parse(match);

  let result = {};
  for (let i = 0; i < matches.length; i++) {
    let season = parseInt(matches[i]["season"]);
    let matchId = matches[i].id;
    if (!result[season]) {
      result[season] = {};
    }
    for (let j = 0; j < deliveries.length; j++) {
      if (matchId === deliveries[j].match_id) {
        let batsman = deliveries[j].batsman;
        let totalRuns = parseInt(deliveries[j].batsman_runs);
        let ball = deliveries[j].ball;

        // Initialize batsman stats for the season if not already
        if (!result[season][batsman]) {
          result[season][batsman] = {
            runsScored: 0,
            ballsFaced: 0,
          };
        }
        result[season][batsman].runsScored += totalRuns;
        if (deliveries[j].wide_runs === "0") {
          result[season][batsman].ballsFaced += 1;
        }
      }
    }
  }
  let strikeRate = {};
  for (let season in result) {
    strikeRate[season] = {};
    for (let batsman in result[season]) {
      let runsScored = result[season][batsman].runsScored;
      let ballsFaced = result[season][batsman].ballsFaced;
      let strikeRateValue = (runsScored / ballsFaced) * 100;
      strikeRate[season][batsman] = strikeRateValue.toFixed(2);
    }
  }

  //converting javascript object to json file
  const jsonString = JSON.stringify(strikeRate);
  const outputFilePath = path.join(
    __dirname,
    "../public/output/calculateStrikeRateBySeason.json"
  );
  fs.writeFileSync(outputFilePath, jsonString);
}

calculateStrikeRateBySeason();
