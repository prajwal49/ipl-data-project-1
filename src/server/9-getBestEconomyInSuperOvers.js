const csvToJson = require("../data/csvToJson.js");
const fs = require("fs");
const path = require("path");

const deliveryPath = path.join(__dirname, "../data/deliveries.csv");

const matchPath = path.join(__dirname, "../data/matches.csv");

async function getBestEconomyInSuperOvers() {
  const dilivery = await csvToJson(deliveryPath);
  const match = await csvToJson(matchPath);

  const deliveries = JSON.parse(dilivery);
  const matches = JSON.parse(match);

  let result = {};
  for (let i = 0; i < deliveries.length; i++) {
    if (deliveries[i].is_super_over == 1) {
      let bowler = deliveries[i].bowler;
      let totalRuns = parseInt(deliveries[i].total_runs);
      let batsmanRuns = parseInt(deliveries[i].batsman_runs);
      let extraRuns = parseInt(deliveries[i].extra_runs);
      let wideRuns = parseInt(deliveries[i].wide_runs);
      let noBallRuns = parseInt(deliveries[i].noball_runs);

      if(!result[bowler]){
        result[bowler] = {
            runsConceded: 0,
            ballsBowled:0
        }
      }
      result[bowler].runsConceded += batsmanRuns;
      if(wideRuns === 0 && noBallRuns === 0){
        result[bowler].ballsBowled += 1;
      }
    }
  }

  let economy = [];
  for (let bowler in result) {
    let runConceded = result[bowler].runsConceded;
    let oversBowled = result[bowler].ballsBowled / 6;
    let economyRate = runConceded / oversBowled;

    economy.push({ bowler: bowler, economyRate: economyRate });
  }
  
  let top10Bowlers = [];
  for (let bowler in economy) {
    top10Bowlers.push([bowler, economy[bowler]]);
    top10Bowlers.sort((a, b) => a[1].economyRate - b[1].economyRate);
  }
  let obj = Object.entries(top10Bowlers[0])
  //converting javascript object to json file
  const jsonString = JSON.stringify(Object.fromEntries(obj));
  const outputFilePath = path.join(
    __dirname,
    "../public/output/getBestEconomyInSuperOvers.json"
  );
  fs.writeFileSync(outputFilePath, jsonString);

}
getBestEconomyInSuperOvers();
