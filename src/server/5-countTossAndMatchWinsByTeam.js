const csvToJson = require("../data/csvToJson.js");
const fs = require("fs");
const path = require("path");

const deliveryPath = path.join(__dirname, "../data/deliveries.csv");

const matchPath = path.join(__dirname, "../data/matches.csv");

async function countTossAndMatchWinsByTeam() {
  try {
    const match = await csvToJson(matchPath);
    const matches = JSON.parse(match);
    let result = {};

    for (let i = 0; i < matches.length; i++) {
      let obj = matches[i];
      if (obj["toss_winner"] === obj["winner"]) {
        if (!result[obj["toss_winner"]]) {
          result[obj["toss_winner"]] = 0;
        }

        result[obj["toss_winner"]] += 1;
      }
    }

    // converting javascript object to json file
    const jsonString = JSON.stringify(result);
    const outputFilePath = path.join(
      __dirname,
      "../public/output/countTossAndMatchWinsByTeam.json"
    );
    fs.writeFileSync(outputFilePath, jsonString);
  } catch (error) {
    console.error("Error in matchesPerYear:", error);
  }
}

countTossAndMatchWinsByTeam();
